/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum layers {
    _COLEMAK_DH = 0,
    _QWERTY = 1,
    _GAMING = 2,
    _LOWER = 3,
    _RAISE = 4,
    _FUNCTION = 5,
    _ADJUST = 6
};


// Aliases for readability
#define QWERTY   DF(_QWERTY)
#define COLEMAK  DF(_COLEMAK_DH)
#define GAMING   DF(_GAMING)

#define RAISE    MO(_RAISE)
#define LOWER    MO(_LOWER)
#define FKEYS    MO(_FUNCTION)
#define ADJUST   MO(_ADJUST)

// Alias for dual keys
#define RS_ENT    LT(RAISE, KC_ENT)
#define RS_SPC    LT(RAISE, KC_SPC)
#define LW_SPC    LT(LOWER, KC_SPC)
#define LW_DEL    LT(LOWER, KC_DEL)
#define FN_TAB    LT(FKEYS, KC_TAB)
#define AD_END    LT(ADJUST, KC_END)
#define AD_CAPS   LT(ADJUST, KC_CAPS)

// Home row mod Colemak
#define HR_A LGUI_T(KC_A)
#define HR_R LALT_T(KC_R)
#define HR_S LSFT_T(KC_S)
#define HR_T LCTL_T(KC_T)
#define HR_N RCTL_T(KC_N)
#define HR_E RSFT_T(KC_E)
#define HR_I LALT_T(KC_I)
#define HR_O RGUI_T(KC_O)


// Home row mod QWERTY
#define QHR_A LGUI_T(KC_A)
#define QHR_S LALT_T(KC_S)
#define QHR_D LSFT_T(KC_D)
#define QHR_F LCTL_T(KC_F)
#define QHR_J RCTL_T(KC_J)
#define QHR_K RSFT_T(KC_K)
#define QHR_L LALT_T(KC_L)
#define QHR_SCLN RGUI_T(KC_SCLN)

// Note: LAlt/Enter (ALT_ENT) is not the same thing as the keyboard shortcut Alt+Enter.
// The notation `mod/tap` denotes a key that activates the modifier `mod` when held down, and
// produces the key `tap` when tapped (i.e. pressed and released).

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
 /*
 * Base Layer: QWERTY
 *
 * ,--------------------------------------------.                                  ,-------------------------------------------.
 * |  Mute/  |   Q  |   W  |   E  |   R  |   T  |                                  |   Y  |   U  |   I  |  O   |  P   |  Print |
 * |  Unmute |      |      |      |      |      |                                  |      |      |      |      |      |  Scrn  |
 * |---------+------+------+------+------+------|                                  |------+------+------+------+------+--------|
 * |  Esc    |   A/ |  S/  |  D/  |   F/ |   G  |                                  |   H  |  J/  |  K/  |  L/  | ; :/ |  Home  |
 * |         | LGUI | LALT | LSFT | LCTL |      |                                  |      | RCTL | RSFT | LATL | RGUI |        |
 * |---------+------+------+------+------+------+----------------.  ,--------------+------+------+------+------+------+--------|
 * | CapsLk/ |   Z  |   X  |   C  |   V  |   B  |   Del  | Tab/Fn|  |   ' " | Bspc |   N  |   M  | ,  < | .  > | / ?  | End/   |
 * | Adjust  |      |      |      |      |      |        |       |  |       |      |      |      |      |      |      | Adjust |
 * `---------+------+------+------+------+------+--------+-------|  |-------+------+------+------+------+----------------------'
 *                         | LGUI | LALT | LSFT | Enter  | LCTL  |  | RCTL  | Space| RSFT | RALT | RGUI |
 *                         |      |      |      |/Raise  |       |  |       |/Lower|      |      |      |
 *                         `-------------------------------------'  `-----------------------------------'
 */
    [_QWERTY] = LAYOUT(
      KC_MUTE , KC_Q ,  KC_W   ,  KC_E  ,   KC_R ,   KC_T ,                                       KC_Y  ,   KC_U ,   KC_I  ,   KC_O ,   KC_P  , KC_PSCR,
      KC_ESC  , QHR_A,  QHR_S  ,  QHR_D ,  QHR_F ,   KC_G ,                                       KC_H  ,  QHR_J ,  QHR_K  ,  QHR_L , QHR_SCLN, KC_HOME,
      AD_CAPS , KC_Z ,  KC_X   ,  KC_C  ,   KC_V ,   KC_B , KC_DEL , FN_TAB ,  KC_QUOT , KC_BSPC, KC_N  ,  KC_M  , KC_COMM , KC_DOT , KC_SLSH , AD_END ,
                                 KC_LGUI, KC_LALT, KC_LSFT, RS_ENT , KC_LCTL,  KC_RCTL , LW_SPC ,KC_RSFT, KC_RALT, KC_RGUI
    ),

/*
 * Base Layer: Colemak DH
 *
 * ,--------------------------------------------.                                  ,-------------------------------------------.
 * |  Mute/  |   Q  |   W  |   F  |   P  |   B  |                                  |   J  |   L  |   U  |  Y   | ; :  |  Print |
 * |  Unmute |      |      |      |      |      |                                  |      |      |      |      |      |  Scrn  |
 * |---------+------+------+------+------+------|                                  |------+------+------+------+------+--------|
 * |  Esc    |   A/ |  R/  |  S/  |   T/ |   G  |                                  |   M  |  N/  |  E/  |  I/  |  O/  |  Home  |
 * |         | LGUI | LALT | LSFT | LCTL |      |                                  |      | RCTL | RSFT | LATL | RGUI |        |
 * |---------+------+------+------+------+------+----------------.  ,--------------+------+------+------+------+------+--------|
 * | CapsLk/ |   Z  |   X  |   C  |   D  |   V  |   Del  | Tab/Fn|  |   ' " | Bspc |   K  |   H  | ,  < | .  > | / ?  | End/   |
 * | Adjust  |      |      |      |      |      |        |       |  |       |      |      |      |      |      |      | Adjust |
 * `---------+------+------+------+------+------+--------+-------|  |-------+------+------+------+------+----------------------'
 *                         | LGUI | LALT | LSFT | Enter  | LCTL  |  | RCTL  | Space| RSFT | RALT | RGUI |
 *                         |      |      |      |/Raise  |       |  |       |/Lower|      |      |      |
 *                         `-------------------------------------'  `-----------------------------------'
 */
    [_COLEMAK_DH] = LAYOUT(
      KC_MUTE , KC_Q ,  KC_W  ,   KC_F  ,   KC_P ,   KC_B ,                                         KC_J  ,  KC_L ,   KC_U  ,  KC_Y ,KC_SCLN, KC_PSCR,
      KC_ESC  , HR_A ,  HR_R  ,   HR_S  ,   HR_T ,   KC_G ,                                         KC_M  ,  HR_N ,   HR_E  ,  HR_I , HR_O  , KC_HOME,
      AD_CAPS , KC_Z ,  KC_X  ,   KC_C  ,   KC_D ,   KC_V , KC_DEL , FN_TAB ,   KC_QUOT  , KC_BSPC, KC_K  ,  KC_H , KC_COMM , KC_DOT,KC_SLSH, AD_END ,
                                 KC_LGUI, KC_LALT, KC_LSFT, RS_ENT , KC_LCTL,   KC_RCTL  , LW_SPC ,KC_RSFT,KC_RALT,  KC_RGUI
    ),

/*
 * Gaming
 *
 * ,--------------------------------------------.                              ,-------------------------------------------.
 * |  Mute/  |   Q  |  F   |  E   |  R   |  T   |                              |   Y  |   U  |   I  |  O   |  P   |  Print |
 * |  Unmute |      |      |      |      |      |                              |      |      |      |      |      |  Scrn  |
 * |---------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |  Esc    |  A   |  S   |  W   |  D   |  G   |                              |   H  |  J   |  K   |  L   |  ; : | Home   |
 * |---------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | CapsLk/ |  Z   |  X   |  C   |  V   |  B   | Enter|Tab/Fn|  |  ' " | Bspc |   N  |  M   |  , < |  . > | / ?  | End/   |
 * | Adjust  |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      | Adjust |
 * `---------+------+------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                         | LGUI | LALT | LSFT | Space| LCTL |  | RCTL | Del  | RSFT | RALT | RGUI |
 *                         |      |      |      |/Raise|      |  |      |/Lower|      |      |      |
 *                         `----------------------------------'  `----------------------------------'
 */
    [_GAMING] = LAYOUT(
      KC_MUTE , KC_Q ,  KC_F   ,  KC_E  ,   KC_R ,   KC_T ,                                       KC_Y  ,  KC_U  ,  KC_I   ,  KC_O  ,  KC_P   , KC_PSCR,
      KC_ESC  , KC_A ,  KC_S   ,  KC_W  ,   KC_D ,   KC_G ,                                       KC_H  ,  KC_J  ,  KC_K   ,  KC_L  , KC_SCLN , KC_HOME,
      AD_CAPS , KC_Z ,  KC_X   ,  KC_C  ,   KC_V ,   KC_B , KC_ENT , FN_TAB ,  KC_QUOT , KC_BSPC, KC_N  ,  KC_M  , KC_COMM , KC_DOT , KC_SLSH , AD_END ,
                                 KC_LGUI, KC_LALT, KC_LSFT, RS_SPC , KC_LCTL,  KC_RCTL , LW_DEL ,KC_RSFT, KC_RALT, KC_RGUI
    ),

/*
 * Nav Layer: Media, navigation (Lower)
 *
 * ,-------------------------------------------.                              ,---------------------------------------.
 * |        |      |      |      |      |      |                              | Insert| Home | End | PgUp | PgDn |    |
 * |--------+------+------+------+------+------|                              |-------+------+-----+------+------+----|
 * |        |  GUI |  Alt | Shift| Ctrl |      |                              | PrtSc |  ←   |   ↑ |   ↓  |  →   |    |
 * |--------+------+------+------+------+------+-------------.  ,-------------+-------+------+-----+------+------+----|
 * |        |      |      |      |      |      |      |      |  |      |      |       |M Prev|Pause|M Next|      |    |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+------------------'
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */
    [_LOWER] = LAYOUT(
      _______, _______, _______, _______, _______, _______,                                     KC_INS , KC_HOME, KC_END , KC_PGUP, KC_PGDN, _______,
      _______, KC_LGUI, KC_LALT, KC_LSFT, KC_LCTL, _______,                                     KC_PSCR, KC_LEFT, KC_UP  , KC_DOWN, KC_RGHT, _______,
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MPRV,KC_PAUSE, KC_MNXT, _______, _______,
                                 _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

/*
 * Sym Layer: Numbers and symbols (Raise)
 *
 * ,-----------------------------------------.                              ,-------------------------------------------.
 * |      |  1   |  2   |  3   |  4   |  5   |                              |   6  |  7   |  8   |  9   |  0   |        |
 * |------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |      |  \   |  |   |   _  |  -   |  ' " |                              |   +  |  =   |  ,   |  .   |  /   |        |
 * |------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |      |   <  |  >   |  (   |  )   |   ~  |      |      |  |      |      |   `  |  [   |  ]   |  {   |  }   |        |
 * `--------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                      |      |      |      |      |      |  |      |      |      |      |      |
 *                      |      |      |      |      |      |  |      |      |      |      |      |
 *                      `----------------------------------'  `----------------------------------'
 */
    [_RAISE] = LAYOUT(
     _______,   KC_1 ,   KC_2 ,   KC_3 ,   KC_4 ,   KC_5 ,                                       KC_6 ,   KC_7 ,   KC_8 ,   KC_9 ,   KC_0 , _______,
     _______, KC_BSLS, KC_PIPE, KC_UNDS, KC_MINS, KC_QUOT,                                     KC_PLUS, KC_EQL , KC_COMM, KC_DOT , KC_SLSH, _______,
     _______, KC_LABK, KC_RABK, KC_LPRN, KC_RPRN, KC_TILD, _______, _______, _______, _______, KC_GRV , KC_LBRC, KC_RBRC, KC_LCBR, KC_RCBR, _______,
                                _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

/*
 * Function Layer: Function keys
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |        |  F1  |  F2  |  F3  |  F4  |      |                              |      | NmLk |CapsLk| SclLk|      |        |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |        |  F5  |  F6  |  F7  |  F8  |      |                              |      | Ctrl | Shift|  Alt |  GUI |        |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |        |  F9  | F10  | F11  | F12  |      |      |      |  |      |      |      |      |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */
    [_FUNCTION] = LAYOUT(
      _______,  KC_F1 ,  KC_F2 ,  KC_F3 ,  KC_F4 , _______,                                     _______, KC_NUM , KC_CAPS, KC_SCRL, _______, _______,
      _______,  KC_F5 ,  KC_F6 ,  KC_F7 ,  KC_F8 , _______,                                     _______, KC_RCTL, KC_RSFT, KC_LALT, KC_RGUI, _______,
      _______,  KC_F9 ,  KC_F10,  KC_F11,  KC_F12, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
                                 _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

/*
 * Adjust Layer: Default layer settings, RGB
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |        |      |QWERTY|Colmak|Gaming|      |                              | TOG  | SAI  | HUI  | VAI  | MOD  |        |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |      |  |      |      |      | SAD  | HUD  | VAD  | RMOD |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */
    [_ADJUST] = LAYOUT(
      _______, _______, _______, _______, _______, _______,                                    _______, _______, _______, _______,  _______, _______,
      _______, _______, QWERTY , COLEMAK, GAMING , _______,                                    RGB_TOG, RGB_SAI, RGB_HUI, RGB_VAI,  RGB_MOD, _______,
      _______, _______, _______, _______, _______, _______,_______, _______, _______, _______, _______, RGB_SAD, RGB_HUD, RGB_VAD, RGB_RMOD, _______,
                                 _______, _______, _______,_______, _______, _______, _______, _______, _______, _______
    ),

// /*
//  * Layer template
//  *
//  * ,-------------------------------------------.                              ,-------------------------------------------.
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      |        |
//  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        `----------------------------------'  `----------------------------------'
//  */
//     [_LAYERINDEX] = LAYOUT(
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
//                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
//     ),
};

/* The default OLED and rotary encoder code can be found at the bottom of qmk_firmware/keyboards/splitkb/kyria/rev1/rev1.c
 * These default settings can be overriden by your own settings in your keymap.c
 * For your convenience, here's a copy of those settings so that you can uncomment them if you wish to apply your own modifications.
 * DO NOT edit the rev1.c file; instead override the weakly defined default functions by your own.
 */


#ifdef OLED_ENABLE
oled_rotation_t oled_init_user(oled_rotation_t rotation) { return OLED_ROTATION_180; }

static void render_kyria_logo(void) {
    // clang-format off
    static const char PROGMEM kyria_logo[] = {
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,192,224,240,112,120, 56, 60, 28, 30, 14, 14, 14,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, 14, 14, 14, 30, 28, 60, 56,120,112,240,224,192,128,128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,192,224,240,124, 62, 31, 15,  7,  3,  1,128,192,224,240,120, 56, 60, 28, 30, 14, 14,  7,  7,135,231,127, 31,255,255, 31,127,231,135,  7,  7, 14, 14, 30, 28, 60, 56,120,240,224,192,128,  1,  3,  7, 15, 31, 62,124,240,224,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,240,252,255, 31,  7,  1,  0,  0,192,240,252,254,255,247,243,177,176, 48, 48, 48, 48, 48, 48, 48,120,254,135,  1,  0,  0,255,255,  0,  0,  1,135,254,120, 48, 48, 48, 48, 48, 48, 48,176,177,243,247,255,254,252,240,192,  0,  0,  1,  7, 31,255,252,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,255,255,255,  0,  0,  0,  0,  0,254,255,255,  1,  1,  7, 30,120,225,129,131,131,134,134,140,140,152,152,177,183,254,248,224,255,255,224,248,254,183,177,152,152,140,140,134,134,131,131,129,225,120, 30,  7,  1,  1,255,255,254,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0,255,255,  0,  0,192,192, 48, 48,  0,  0,240,240,  0,  0,  0,  0,  0,  0,240,240,  0,  0,240,240,192,192, 48, 48, 48, 48,192,192,  0,  0, 48, 48,243,243,  0,  0,  0,  0,  0,  0, 48, 48, 48, 48, 48, 48,192,192,  0,  0,  0,  0,  0,
        0,  0,  0,255,255,255,  0,  0,  0,  0,  0,127,255,255,128,128,224,120, 30,135,129,193,193, 97, 97, 49, 49, 25, 25,141,237,127, 31,  7,255,255,  7, 31,127,237,141, 25, 25, 49, 49, 97, 97,193,193,129,135, 30,120,224,128,128,255,255,127,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0, 63, 63,  3,  3, 12, 12, 48, 48,  0,  0,  0,  0, 51, 51, 51, 51, 51, 51, 15, 15,  0,  0, 63, 63,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 48, 48, 63, 63, 48, 48,  0,  0, 12, 12, 51, 51, 51, 51, 51, 51, 63, 63,  0,  0,  0,  0,  0,
        0,  0,  0,  0, 15, 63,255,248,224,128,  0,  0,  3, 15, 63,127,255,239,207,141, 13, 12, 12, 12, 12, 12, 12, 12, 30,127,225,128,  0,  0,255,255,  0,  0,128,225,127, 30, 12, 12, 12, 12, 12, 12, 12, 13,141,207,239,255,127, 63, 15,  3,  0,  0,128,224,248,255, 63, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  3,  7, 15, 62,124,248,240,224,192,128,  1,  3,  7, 15, 30, 28, 60, 56,120,112,112,224,224,225,231,254,248,255,255,248,254,231,225,224,224,112,112,120, 56, 60, 28, 30, 15,  7,  3,  1,128,192,224,240,248,124, 62, 15,  7,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  3,  7, 15, 14, 30, 28, 60, 56,120,112,112,112,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,112,112,112,120, 56, 60, 28, 30, 14, 15,  7,  3,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
    };
    // clang-format on
    oled_write_raw_P(kyria_logo, sizeof(kyria_logo));
}

static void render_status(void) {
    // QMK Logo and version information
    // clang-format off
    static const char PROGMEM qmk_logo[] = {
        0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
        0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
        0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};
    // clang-format on

    oled_write_P(qmk_logo, false);
    oled_write_P(PSTR("Kyria rev1.0\n\n"), false);

    // Host Keyboard Layer Status
    oled_write_P(PSTR("Layer: "), false);
    switch (get_highest_layer(layer_state|default_layer_state)) {
        case _QWERTY:
            oled_write_P(PSTR("QWERTY\n"), false);
            break;
        case _COLEMAK_DH:
            oled_write_P(PSTR("Colemak-DH\n"), false);
            break;
        case _GAMING:
            oled_write_P(PSTR("Gaming\n"), false);
            break;
        case _LOWER:
            oled_write_P(PSTR("Lower\n"), false);
            break;
        case _RAISE:
            oled_write_P(PSTR("Raise\n"), false);
            break;
        case _FUNCTION:
            oled_write_P(PSTR("Function\n"), false);
            break;
        case _ADJUST:
            oled_write_P(PSTR("Adjust\n"), false);
            break;
        default:
            oled_write_P(PSTR("Undefined\n"), false);
    }

    // Write host Keyboard LED Status to OLEDs
    led_t led_usb_state = host_keyboard_led_state();
    oled_write_P(led_usb_state.num_lock    ? PSTR("NUMLCK ") : PSTR("       "), false);
    oled_write_P(led_usb_state.caps_lock   ? PSTR("CAPLCK ") : PSTR("       "), false);
    oled_write_P(led_usb_state.scroll_lock ? PSTR("SCRLCK ") : PSTR("       "), false);
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
        render_status(); // Renders the current keyboard state (layer, lock, caps, scroll, etc)
    } else {
        render_kyria_logo();
    }
    return false;
}
#endif

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {

    if (index == 0) {
        // Volume control
        if (clockwise) {
            tap_code(KC_VOLD);
        } else {
            tap_code(KC_VOLU);
        }
    } else if (index == 1) {
        // Page up/Page down
        if (clockwise) {
            tap_code(KC_PGUP);
        } else {
            tap_code(KC_PGDN);
        }
    }
    return false;
}
#endif
