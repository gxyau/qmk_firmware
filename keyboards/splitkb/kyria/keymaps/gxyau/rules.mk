OLED_ENABLE = yes
OLED_DRIVER = ssd1306     # Enables the use of OLED displays
ENCODER_ENABLE  = yes     # Enables the use of one or more encoders
RGBLIGHT_ENABLE = yes     # Enable keyboard RGB underglow
CONVERT_TO  = liatris 	  # Convert the firmware to UF2 file, compatible with RP2040
# MOUSEKEY_ENABLE = yes     # Enable Wheel Up and Down
# EXTRAKEY_ENABLE = yes     # Enabling media keys
